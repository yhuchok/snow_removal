@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('sidebar')

            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Job {{ $job->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/jobs/assign/' . $job->id) }}" title="Assign Job"><button class="btn btn-secondary btn-sm">Assign</button></a>
                        <a href="{{ url('/jobs/' . $job->id . '/edit') }}" title="Edit Job"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('jobs' . '/' . $job->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Job" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $job->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Title</th>
                                        <td>{{ $job->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Content</th>
                                        <td>{{ $job->content }}</td>
                                    </tr>
                                    <tr>
                                        <th>Laborer</th>
                                        <td>
                                            {{ $job->laborer ? $job->laborer->first_name . ' ' . $job->laborer->last_name : '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Client</th>
                                        <td>
                                            {{ $job->client ? $job->client->first_name . ' ' . $job->client->last_name : '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Cost</th>
                                        <td>${{ $job->cost ? $job->cost : 0 }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>{{ $job->status }}</td>
                                    </tr>
                                    <tr>
                                        <th>Expected date</th>
                                        <td>{{ $job->expected_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
