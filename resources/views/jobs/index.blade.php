@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('sidebar')

            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Jobs</div>
                    <div class="card-body">
                        <a href="{{ url('/jobs/create') }}" class="btn btn-success btn-sm" title="Add New Job">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/jobs') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="hidden" class="form-control" name="client_id" value="{{ Request::get('client_id') }}">
                                <input type="hidden" class="form-control" name="laborer_id" value="{{ Request::get('laborer_id') }}">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>

                                    <tr>
                                        <th>#</th>
                                        <th width="100px">Client</th>
                                        <th>City</th>
                                        <th>Expected Date</th>
                                        <th>Time</th>
                                        <th width="5px">Type</th>
                                        <th>Status</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($jobs as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->client ? $item->client->first_name . ' ' . $item->client->last_name : '' }}</td>
                                        <td>{{ $item->client ? $item->client->city : '' }}</td>
                                        <td>{{ $item->expected_at->toDateString() }}</td>
                                        <td>{{ $item->created_at->toDateString() }}</td>
                                        <td>{{ $item->client && $item->client->subscription_type ? $item->client->subscription_type->name : '' }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td class="text-right">
                                            <a href="{{ url('/jobs/assign/' . $item->id) }}" title="Assign Job"><button class="btn btn-secondary btn-sm">Assign</button></a>
                                            <a href="{{ url('/clients/' . $item->client->id) }}" title="View Client"><button class="btn btn-info btn-sm"><i class="fa fa-user" aria-hidden="true"></i> </button></a>
                                            <a href="{{ url('/jobs/' . $item->id) }}" title="View Job"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                            <a href="{{ url('/jobs/' . $item->id . '/edit') }}" title="Edit Job"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button></a>

                                            <form method="POST" action="{{ url('/jobs' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Job" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $jobs->appends([
                                'search' => Request::get('search'),
                                'client_id' => Request::get('client_id'),
                                'laborer_id' => Request::get('laborer_id')
                            ])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
