<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($job->title) ? $job->title : ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    <label for="content" class="control-label">{{ 'Content' }}</label>
    <textarea class="form-control" rows="5" name="content" type="textarea" id="content" >{{ isset($job->content) ? $job->content : ''}}</textarea>
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('client_id') ? 'has-error' : ''}}">
    <label for="client_id" class="control-label">{{ 'Client' }}</label>
    
    <select name="client_id" class="form-control">
        @foreach ($clients as $c)
        <option value="{{ $c->id }}" {{ (isset($job->client->id) && $job->client->id == $c->id) ? 'selected' : ''}}>{{ $c->first_name }} {{ $c->last_name }}</option>
        @endforeach
    </select>
    {!! $errors->first('client_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Cost' }} ($)</label>
    <input class="form-control" name="cost" type="text" id="title" value="{{ isset($job->cost) ? $job->cost : '' }}" >
    {!! $errors->first('cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Expected date' }}</label>
    <input autocomplete="off" class="form-control datepicker" type="text" name="expected_at" value="{{ isset($job->expected_at) ? $job->expected_at->toDateString() : '' }}">
    {!! $errors->first('cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
