<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    <label for="first_name" class="control-label">{{ 'First Name' }}</label>
    <input class="form-control" name="first_name" type="text" id="first_name" value="{{ isset($laborer->first_name) ? $laborer->first_name : ''}}" >
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    <label for="last_name" class="control-label">{{ 'Last Name' }}</label>
    <input class="form-control" name="last_name" type="text" id="last_name" value="{{ isset($laborer->last_name) ? $laborer->last_name : ''}}" >
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="control-label">{{ 'Address' }}</label>
    <input class="form-control" name="address" type="text" id="address" value="{{ isset($laborer->address) ? $laborer->address : ''}}" >
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    <label for="city" class="control-label">{{ 'City' }}</label>
    <input class="form-control" name="city" type="text" id="city" value="{{ isset($laborer->city) ? $laborer->city : ''}}" >
    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
    <label for="state" class="control-label">{{ 'State' }}</label>
    <input class="form-control" name="state" type="text" id="state" value="{{ isset($laborer->state) ? $laborer->state : ''}}" >
    {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}}">
    <label for="zip" class="control-label">{{ 'Zip' }}</label>
    <input class="form-control" name="zip" type="text" id="zip" value="{{ isset($laborer->zip) ? $laborer->zip : ''}}" >
    {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($laborer->email) ? $laborer->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($laborer->phone) ? $laborer->phone : ''}}" >
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('transport_method') ? 'has-error' : ''}}">
    <label for="transport_method" class="control-label">{{ 'Transport Status' }}</label>
    <select name="transport_method" class="form-control" id="transport_method" >
        <option value="" {{ (isset($laborer->transport_method) && $laborer->transport_method == '') ? 'selected' : ''}}>- Select -</option>
        <option value="Has car" {{ (isset($laborer->transport_method) && $laborer->transport_method == 'Has car') ? 'selected' : ''}}>Has car</option>
        <option value="No Car" {{ (isset($laborer->transport_method) && $laborer->transport_method == 'No Car') ? 'selected' : ''}}>No Car</option>
        <option value="On crew" {{ (isset($laborer->transport_method) && $laborer->transport_method == 'On crew') ? 'selected' : ''}}>On crew</option>
    </select>
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('jmile') ? 'has-error' : ''}}">
    <label for="jmile" class="control-label">{{ 'jmile' }}</label>
    <input class="form-control" name="jmile" type="text" id="jmile" value="{{ isset($laborer->jmile) ? $laborer->jmile : ''}}" >
    {!! $errors->first('jmile', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('jmin') ? 'has-error' : ''}}">
    <label for="jmin" class="control-label">{{ 'jmin' }}</label>
    <input class="form-control" name="jmin" type="text" id="jmin" value="{{ isset($laborer->jmin) ? $laborer->jmin : ''}}" >
    {!! $errors->first('jmin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('jtime') ? 'has-error' : ''}}">
    <label for="jtime" class="control-label">{{ 'jtime' }}</label>
    <input class="form-control" name="jtime" type="text" id="jtime" value="{{ isset($laborer->jtime) ? $laborer->jtime : ''}}" >
    {!! $errors->first('jtime', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('jpay') ? 'has-error' : ''}}">
    <label for="jpay" class="control-label">{{ 'jpay' }}</label>
    <input class="form-control" name="jpay" type="text" id="jpay" value="{{ isset($laborer->jpay) ? $laborer->jpay : ''}}" >
    {!! $errors->first('jpay', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
