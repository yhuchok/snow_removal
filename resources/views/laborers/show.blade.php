@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Laborer {{ $laborer->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/laborers/' . $laborer->id . '/edit') }}" title="Edit Laborer">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('laborers' . '/' . $laborer->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Laborer" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $laborer->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>First Name</th>
                                        <td>{{ $laborer->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Last Name</th>
                                        <td>{{ $laborer->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>City</th>
                                        <td>{{ $laborer->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td>{{ $laborer->address }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <td>{{ $laborer->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $laborer->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Transport Status</th>
                                        <td>{{ $laborer->transport_method }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>{{ $laborer->status }}</td>
                                    </tr>
                                    <tr>
                                        <th>jmile</th>
                                        <td>{{ $laborer->jmile }}</td>
                                    </tr>
                                    <tr>
                                        <th>jmin</th>
                                        <td>{{ $laborer->jmin }}</td>
                                    </tr>
                                    <tr>
                                        <th>jtime</th>
                                        <td>{{ $laborer->jtime }}</td>
                                    </tr>
                                    <tr>
                                        <th>jpay</th>
                                        <td>{{ $laborer->jpay }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card mt-4">
                    <div class="card-header">Active jobs</div>
                    <div class="card-body">
                        <a href="{{ url('/jobs/?laborer_id=' . $laborer->id) }}" title="View all laborer jobs">
                            <button class="btn btn-primary btn-sm">View all</button>
                        </a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Laborer</th>
                                        <th>Status</th>
                                        <th>Cost</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($jobs as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->laborer ? $item->laborer->first_name . ' ' . $item->laborer->last_name : '' }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>{{ !empty($item->cost) ? '$' . $item->cost : '' }}
                                        <td class="text-right">
                                            <a href="{{ url('/jobs/' . $item->id) }}" title="View Job"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/jobs/' . $item->id . '/edit') }}" title="Edit Job"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/jobs' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Job" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
