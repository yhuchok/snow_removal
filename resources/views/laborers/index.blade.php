@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('sidebar')

            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        @if(!$assign_job)
                            Laborers
                        @else
                            Assign job #{{ $assign_job->id }}
                        @endif
                    </div>
                    <div class="card-body">
                        @if(!$assign_job)
                            <a href="{{ url('/laborers/create') }}" class="btn btn-success btn-sm" title="Add New Laborer">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        @endif

                        <form method="GET" action="{{ url('/laborers') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="hidden" class="form-control" name="assign_job" value="{{ request('assign_job') }}">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>City</th>
                                            <th>Status</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($laborers as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                                            <td>{{ $item->city }}</td>
                                            <td>{{ $item->status }}</td>
                                          
                                            <td class="text-right">
                                                @if(!$assign_job)
                                                    <a title="Send text message to laborer" onclick="alert('Feature not yet implemented.');"><button class="btn btn-info btn-sm"><i class="fa fa-envelope" aria-hidden="true"></i> Message</button></a>
                                                    <a href="{{ url('/jobs/?laborer_id=' . $item->id) }}" title="Show client's jobs"><button class="btn btn-info btn-sm"><i class="fa fa-wrench" aria-hidden="true"></i> Jobs</button></a>
                                                    <a href="{{ url('/laborers/' . $item->id) }}" title="View Laborer"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                    <a href="{{ url('/laborers/' . $item->id . '/edit') }}" title="Edit Laborer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                                    <form method="POST" action="{{ url('/laborers' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Laborer" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                                    </form>
                                                @else
                                                    <form method="POST" action="{{ url('/jobs/assign/' . $assign_job->id . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('POST') }}
                                                        {{ csrf_field() }}

                                                        <button type="submit" class="btn btn-secondary btn-sm" title="Assing job" onclick="return confirm(&quot;Confirm assignment?&quot;)">Assign</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            <div class="pagination-wrapper"> {!! $laborers->appends([
                                'search' => Request::get('search'),
                                'assign_job' => Request::get('assign_job')
                            ])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
