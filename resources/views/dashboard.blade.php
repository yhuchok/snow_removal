@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Clients</div>
                    <div class="card-body">
                        <a href="{{ url('/clients/create') }}" class="btn btn-success btn-sm" title="Add New Client">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <a href="{{ url('/clients/') }}" class="btn btn-info btn-sm" title="View all clients">
                            View all
                        </a>

                        <form method="GET" action="{{ url('/clients') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Go
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Type</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                                        <td>{{ $item->city }}</td>
                                        <td>{{ $item->subscription_type->name }}</td>
                                        <td class="text-right">
                                            <a title="Send text message to client" onclick="alert('Feature not yet implemented.');"><button class="btn btn-info btn-sm"><i class="fa fa-envelope" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/jobs/?client_id=' . $item->id) }}" title="Show client's jobs"><button class="btn btn-info btn-sm"><i class="fa fa-wrench" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/clients/' . $item->id) }}" title="View Client"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $clients->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Jobs</div>
                    <div class="card-body">
                        <a href="{{ url('/jobs/create') }}" class="btn btn-success btn-sm" title="Add New Job">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <a href="{{ url('/jobs/') }}" class="btn btn-info btn-sm" title="View all clients">
                            View all
                        </a>

                        <form method="GET" action="{{ url('/jobs') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Go
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width="100px">Client</th>
                                        <th>City</th>
                                        <th width="5px">Type</th>
                                        <th>Status</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($jobs as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->client->first_name }} {{ $item->client->last_name }}</td>
                                        <td>{{ $item->client->city }}</td>
                                        <td>{{ substr($item->client->subscription_type->name, 0, 1) }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td class="text-right">
                                            <a href="{{ url('/clients/' . $item->client->id) }}" title="View Client"><button class="btn btn-info btn-sm"><i class="fa fa-user" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/jobs/' . $item->id) }}" title="View Job"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $jobs->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-4">
                <div class="card">
                        <div class="card-header">Laborers</div>
                        <div class="card-body">
                            <a href="{{ url('/laborers/create') }}" class="btn btn-success btn-sm" title="Add New Laborer">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>

                            <a href="{{ url('/laborers/') }}" class="btn btn-info btn-sm" title="View all clients">
                                View all
                            </a>
    
                            <form method="GET" action="{{ url('/laborers') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            Go
                                        </button>
                                    </span>
                                </div>
                            </form>
    
                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Current Location</th>
                                            <th>City</th>
                                            <th>Status</th>
                                            <th>Active Jobs</th>
                                            <th>Transport Status</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($laborers as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->first_name }} {{ $item->last_name }}</td>
                                            <td>{{ $item->parseLocation() }}
                                            <td>{{ $item->city }}</td>
                                            <td>{{ $item->status }}</td>
                                            <td>
                                                @foreach($item->jobs as $j)
                                                    <a href="{{ url('/jobs/' . $j->id) }}">#{{ $j->id }}</a>
                                                @endforeach
                                            </td>
                                            <td>{{ $item->transport_method }}</td>
                                            <td class="text-right">
                                                <a title="Send text message to laborer" onclick="alert('Feature not yet implemented.');"><button class="btn btn-info btn-sm"><i class="fa fa-envelope" aria-hidden="true"></i></button></a>
                                                <a href="{{ url('/jobs/?laborer_id=' . $item->id) }}" title="View active jobs"><button class="btn btn-info btn-sm"><i class="fa fa-wrench" aria-hidden="true"></i></button> </a>
                                                <a href="{{ url('/laborers/' . $item->id) }}" title="View Laborer"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $laborers->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
