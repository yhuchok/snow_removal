@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('sidebar', [
                'back_url' => url('/clients/' . $client->id)
            ])

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Client #{{ $client->id }}</div>
                    <div class="card-body">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/clients/' . $client->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('clients.form', ['formMode' => 'edit'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
