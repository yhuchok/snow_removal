<div class="col-md-2">
    <ul class="nav mt-1" role="tablist">
        <a href="{{ !empty($back_url) ? $back_url : url('/') }}" title="Back" class="btn-block">
            <button class="btn btn-warning btn-block"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
        </a>
    </ul>
</div>
