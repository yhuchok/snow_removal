<?php

use Illuminate\Database\Seeder;
use App\Models\SubscriptionType;

class SubscriptionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['name' => 'Single'],
            ['name' => 'Monthly'],
            ['name' => 'VIP']
        ];

        foreach ($types as $type) {
            SubscriptionType::create($type);
        }
    }
}
