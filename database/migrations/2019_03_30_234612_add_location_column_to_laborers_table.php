<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationColumnToLaborersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laborers', function (Blueprint $table) {
            $table->decimal('location_lat', 10, 8)->nullable();
            $table->decimal('location_lon', 11, 8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laborers', function (Blueprint $table) {
            $table->dropColumn('location_lat');
            $table->dropColumn('location_lon');
        });
    }
}
