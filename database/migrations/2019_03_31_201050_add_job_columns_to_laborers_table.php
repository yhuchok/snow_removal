<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobColumnsToLaborersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laborers', function (Blueprint $table) {
            $table->string('jmile')->nullable();
            $table->string('jmin')->nullable();
            $table->string('jtime')->nullable();
            $table->string('jpay')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laborers', function (Blueprint $table) {
            $table->dropColumn([
                'jmile',
                'jmin',
                'jtime',
                'jpay'
            ]);
        });
    }
}
