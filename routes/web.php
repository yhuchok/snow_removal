<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Auth::routes();

Route::resource('clients', 'ClientsController');
Route::resource('laborers', 'LaborersController');
Route::resource('jobs', 'JobsController');

Route::get('jobs/assign/{job}', 'JobsController@assign');
Route::post('jobs/assign/{job}/{laborer}', 'JobsController@doAssign');
