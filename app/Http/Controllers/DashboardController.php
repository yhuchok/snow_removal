<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Laborer;
use App\Models\Job;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagination = 10;

        $clients = Client::latest()->paginate($pagination);
        $laborers = Laborer::latest()->paginate($pagination);
        $jobs = Job::latest()->paginate($pagination);

        return view('dashboard', compact('clients', 'laborers', 'jobs'));
    }
}
