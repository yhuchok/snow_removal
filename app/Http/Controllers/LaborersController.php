<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\Models\Laborer;
use App\Models\Job;
use Illuminate\Http\Request;

class LaborersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $laborers = Laborer::where('first_name', 'LIKE', "%$keyword%")
                ->orWhere('last_name', 'LIKE', "%$keyword%")
                ->orWhere('address', 'LIKE', "%$keyword%")
                ->orWhere('city', 'LIKE', "%$keyword%")
                ->orWhere('state', 'LIKE', "%$keyword%")
                ->orWhere('zip', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->orWhere('transport_method', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $laborers = Laborer::latest()->paginate($perPage);
        }

        $assign_job = $request->get('assign_job');
        if (!empty($assign_job)) {
            $assign_job = Job::find($assign_job);
        }

        return view('laborers.index', compact('laborers', 'assign_job'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('laborers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Laborer::create($requestData);

        return redirect('laborers')->with('flash_message', 'Laborer added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $laborer = Laborer::findOrFail($id);
        $jobs = new Collection();

        if ($laborer) {
            $jobs = $laborer->jobs()->latest()->active()->get();
        }

        return view('laborers.show', compact('laborer', 'jobs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $laborer = Laborer::findOrFail($id);

        return view('laborers.edit', compact('laborer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $laborer = Laborer::findOrFail($id);
        $laborer->update($requestData);

        return redirect('laborers')->with('flash_message', 'Laborer updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Laborer::destroy($id);

        return redirect('laborers')->with('flash_message', 'Laborer deleted!');
    }
}
