<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreJob;
use App\Models\Job;
use App\Models\Client;
use App\Models\Laborer;
use Illuminate\Http\Request;
use Carbon\Carbon;

class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $jobs = Job::where('title', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest();
        } else {
            $jobs = Job::latest();
        }

        $client_id = $request->get('client_id');
        $laborer_id = $request->get('laborer_id');

        if (!empty($client_id)) {
            $jobs->where('client_id', $client_id);
        }

        if (!empty($laborer_id)) {
            $jobs->where('laborer_id', $laborer_id);
        }

        $jobs = $jobs->paginate($perPage);

        return view('jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $clients = Client::all();

        return view('jobs.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreJob $request)
    {
        $requestData = $request->all();
        
        if (!empty($requestData['expected_at'])) {
            $requestData['expected_at'] = Carbon::parse($requestData['expected_at']);
        }

        $job = new Job($requestData);

        $job->status = 'not_assigned';

        $job->save();

        return redirect('jobs')->with('flash_message', 'Job added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $job = Job::findOrFail($id);
        $clients = Client::all();

        return view('jobs.edit', compact('job', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(StoreJob $request, $id)
    {
        $requestData = $request->all();
        
        if (!empty($requestData['expected_at'])) {
            $requestData['expected_at'] = Carbon::parse($requestData['expected_at']);
        }

        $job = Job::findOrFail($id);
        $job->update($requestData);

        return redirect('jobs')->with('flash_message', 'Job updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Job::destroy($id);

        return redirect('jobs')->with('flash_message', 'Job deleted!');
    }

    /**
     * Load job assignment interface.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function assign($id)
    {
        return redirect()->route(
            'laborers.index', ['assign_job' => $id]
        );
    }

    /**
     * Assign a job to a laborer.
     *
     * @param  Job $job
     * @param  Laborer $laborer
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doAssign(Job $job, Laborer $laborer)
    {
        $message = 'Job could not be assigned';
        
        if ($job && $laborer) {
            $job->assign($laborer);
            $job->save();
        }

        return redirect('jobs/' . $job->id)->with('flash_message', 'Job assigned!');
    }
}
