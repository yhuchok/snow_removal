<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    const STATUSES = [
        'completed',
        'in_progress',
        'not_assigned'
    ];

    /**
     * Additional dates columns.
     *
     * @var array
     */
    protected $dates = ['expected_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'client_id', 'laborer_id', 'cost', 'expected_at'
    ];

    /**
     * Get job client.
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    /**
     * Get job laborer.
     */
    public function laborer()
    {
        return $this->belongsTo('App\Models\Laborer');
    }

    /**
     * 'status' attribute mutator.
     * 
     * @param  string $status
     * @return string
     */
    public function setStatusAttribute($status = '')
    {
        if (!in_array($status, static::STATUSES)) {
            $status = null;
        }

        if (!empty($status)) {
            $this->attributes['status'] = $status;
        }
    }

    /**
     * Scope a query to only include active jobs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'in_progress');
    }

    /**
     * Assign this job to a particular laborer.
     * 
     * @param  Laborer $laborer
     * @return void
     */
    public function assign(Laborer $laborer)
    {
        $result = false;

        if ($laborer->id) {
            $this->laborer_id = $laborer->id;
            $this->status = 'in_progress';
            $result = true;
        }

        return $result;
    }
}
