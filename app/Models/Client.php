<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'address', 'city', 'state', 'zip', 'email', 'phone', 'subscription_type_id'
    ];

    /**
     * Get client subscription type.
     */
    public function subscription_type()
    {
        return $this->belongsTo(SubscriptionType::class);
    }

    /**
     * Get client jobs.
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}
