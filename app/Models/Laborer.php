<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Laborer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'laborers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'address', 'city', 'state', 'zip', 'email', 'phone', 'transport_method', 'jmile', 'jpay', 'jmin', 'jtime'
    ];

    /**
     * Get currently assigned jobs for this laborer.
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    /**
     * Get status of laborer.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        $status = 'available';

        $jobs = $this->jobs()->active()->get();

        if (!$jobs->isEmpty()) {
            $status = 'occupied';
        }

        return $status;
    }

    /**
     * Parse location to human readable format.
     */
    public function parseLocation()
    {
        $lat = $this->location_lat ?: 0;
        $lon = $this->location_lon ?: 0;

        // TODO. Implement.
        $location = 'location_' . $lat . ':' . $lon;

        return $location;
    }
}
